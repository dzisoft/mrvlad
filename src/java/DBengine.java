
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class DBengine {
    
      static  Session s = HibernateUtil.createSessionFactory().openSession();
    static Transaction t = null;
    
   public static void insertNew(String name, String club){
        KlasaZaHiber KZH = new KlasaZaHiber(name, club);
        try {
            t = s.beginTransaction();
            s.persist(KZH);
                                        System.out.println(KZH);
            t.commit();}
        catch (HibernateException ex) {
            if(t != null){
                t.rollback();}
            System.out.println(ex);}
        finally {
            HibernateUtil.close();}
    }
    
    
}
