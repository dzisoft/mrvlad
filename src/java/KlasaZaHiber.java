
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabla")
public class KlasaZaHiber {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column (name ="name")
    String name;
    @Column (name = "club")
    String club;
   
    public KlasaZaHiber(){    }
    
    public KlasaZaHiber(String name, String club) {
        this.name = name;
        this.club = club;
    }

    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getClub() {return club;}
    public void setClub(String club) {this.club = club;}

    @Override
    public String toString() {return "id:"+this.id+"name:"+this.name+"club:"+this.club;}
    
    
}
